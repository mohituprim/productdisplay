import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CurrencyService {
  constructor(private http: HttpClient) {
  }

  getRates(base:string) {
    let apiUrl = `https://api.exchangeratesapi.io/latest`;
    let RATES = [];
    apiUrl = `${apiUrl}?base=${base}`;

    return this.http.get<any>(apiUrl);
  };
}