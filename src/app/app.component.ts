import { Component, OnInit } from '@angular/core';
import { CurrencyService } from './currency.service';

export interface Currency {
  value: string;
  viewValue: string;
}

export interface Product {
  name: string;
  imageLink: string;
  price: number;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [CurrencyService]
})
export class AppComponent  implements OnInit{
  constructor(private currencyService: CurrencyService) { 
  }

  ngOnInit() {
  }
  rate=1;

  currencies: Currency[] = [
    {value: 'INR', viewValue: 'INR'},
    {value: 'USD', viewValue: 'USD'}
  ];

  products: Product[] = [
    {name: 'Head Phone', imageLink: 'assets/photo01.jpg', price:80},
    {name: 'Wrist Watch', imageLink: 'assets/photo02.jpg', price:90},
    {name: 'Running Shoes', imageLink: 'assets/photo03.jpg', price:100},
  ];
  selected = 'INR';
  getRate(): number {
    return this.rate;
  }
  setRate(crncy:string): void {
    if(crncy=='USD'){
      this.currencyService.getRates('INR')
      .subscribe(data => {
        this.rate=data.rates['USD'];     
        console.log(data.rates['USD']);
      });
    }
  else
    this.rate=1;

    }
}
